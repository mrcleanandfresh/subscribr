<?php

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require_once "../autoload.php";

$entities = array(__DIR__ . ".." . DIRECTORY_SEPARATOR . "src" . DIRECTORY_SEPARATOR . "Subscribr" . DIRECTORY_SEPARATOR . "Entity");
$isDevMode = true;

$connection = array(
	'driver'   => 'pdo_mysql',
	'user'     => 'root',
	'password' => 'password',
	'dbname'   => 'subscribr'
);
$config = Setup::createAnnotationMetadataConfiguration( $entities, $isDevMode );
//$config = Setup::createAnnotationMetadataConfiguration( $entities, $isDevMode, null, null, false );

$entityManager = EntityManager::create( $connection, $config );