<?php
/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 11/28/2016
 * Time: 3:00 PM
 */
require_once "autoload.php";

use Subscribr\Entity\Subscriber;
use Subscribr\Subscribr;

$entityManager = Subscribr::getEntityManager();

$showInterests = isset( $argv[1] ) ? $argv[1] : null;

$subscriberRepo = $entityManager->getRepository( Subscriber::class );
$subscribers = $subscriberRepo->findAll();

if ($showInterests == '--i') {
	foreach ($subscribers as $subscriber) {
		echo sprintf( "-%s\n", $subscriber->getEmail() );
		foreach ($subscriber->getInterests() as $interest) {
			echo sprintf( "--%s\n", $interest->getInterest()->getName() );
		}
	}
} else {
	foreach ($subscribers as $subscriber) {
		echo sprintf( "-%s\n", $subscriber->getEmail() );
	}
}

