<?php
/**
 * Class Autoloader
 */
// Composer autoloader
require_once "vendor/autoload.php";
// Custom autoloader
spl_autoload_register('classloader');
function classloader( $class_name ) {
	if (false !== strpos($class_name, 'Subscribr')) {
		$classes_dir = realpath( __DIR__ ) . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR;
		$class_file = str_replace('\\', DIRECTORY_SEPARATOR, $class_name ).'.php';

		require_once $classes_dir.$class_file;
	}
}