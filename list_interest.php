<?php
/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 11/28/2016
 * Time: 3:00 PM
 */
require_once "autoload.php";

use Subscribr\Entity\Interest;
use Subscribr\Subscribr;

$entityManager = Subscribr::getEntityManager();

$interestRepo = $entityManager->getRepository( Interest::class );
/**
 * @var $interests Interest
 */
$interests = $interestRepo->findAll();

foreach ($interests as $interest) {
	echo sprintf( "-%s\n", $interest->getName() );
}