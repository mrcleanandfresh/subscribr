<?php
/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 11/28/2016
 * Time: 7:46 AM
 */
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Subscribr\Subscribr;

require_once "autoload.php";

$entityManager = Subscribr::getEntityManager();

return ConsoleRunner::createHelperSet($entityManager);