<?php
/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 11/28/2016
 * Time: 3:04 PM
 */
require_once "autoload.php";

use Subscribr\Entity\Subscription;
use Subscribr\Subscribr;

$entityManager = Subscribr::getEntityManager();

$subscriptionRepo = $entityManager->getRepository( Subscription::class );
$subscriptions = $subscriptionRepo->findAll();

foreach ($subscriptions as $subscription) {
	$subscriptionSubscriber = $subscription->getSubscriber();
    echo sprintf( "-%s\n", $subscriptionSubscriber->getEmail() );
    foreach ($subscriptionSubscriber->getInterests() as $interest) {
        echo sprintf( "--%s\n", $interest->getInterest()->getName() );
    }
}