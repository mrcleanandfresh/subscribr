<?php
/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 11/28/2016
 * Time: 8:30 AM
 */

namespace Subscribr\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Subscriber
 * @ORM\Entity()
 * @ORM\Table(name="subscriber", uniqueConstraints={
 *   @ORM\UniqueConstraint(name="subscriber_email", columns={"email"})
 * })
 */
class Subscriber {
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="first_name", type="string")
	 */
	private $first_name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="last_name", type="string")
	 */
	private $last_name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="email", type="string", unique=TRUE, nullable=FALSE)
	 */
	private $email;

	/**
	 * @var array
	 * 
	 * @ORM\OneToMany(targetEntity="Subscription", mappedBy="subscriber", cascade={"persist", "remove"}, orphanRemoval=TRUE)
	 */
	private $subscriptions;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="is_subscribed", type="boolean")
	 */
	private $is_subscribed;

	public function __construct() {
		$this->subscriptions = new ArrayCollection();
	}

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Subscriber
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Subscriber
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Subscriber
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set isSubscribed
     *
     * @param boolean $isSubscribed
     *
     * @return Subscriber
     */
    public function setIsSubscribed($isSubscribed)
    {
        $this->is_subscribed = $isSubscribed;

        return $this;
    }

    /**
     * Get isSubscribed
     *
     * @return boolean
     */
    public function getIsSubscribed()
    {
        return $this->is_subscribed;
    }

    /**
     * Add subscription
     *
     * @param \Subscribr\Entity\Subscription $subscription
     *
     * @return Subscriber
     */
    public function addSubscription(\Subscribr\Entity\Subscription $subscription)
    {
        $this->subscriptions[] = $subscription;

        return $this;
    }

    /**
     * Remove subscription
     *
     * @param \Subscribr\Entity\Subscription $subscription
     */
    public function removeSubscription(\Subscribr\Entity\Subscription $subscription)
    {
        $this->subscriptions->removeElement($subscription);
    }

    /**
     * Get subscriptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubscriptions()
    {
        return $this->subscriptions;
    }
}
