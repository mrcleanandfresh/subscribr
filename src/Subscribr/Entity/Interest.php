<?php
/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 11/28/2016
 * Time: 1:53 PM
 */

namespace Subscribr\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Interest
 * @ORM\Entity()
 * @ORM\Table(name="interest", uniqueConstraints={
 *   @ORM\UniqueConstraint(name="interest_name", columns={"name"})
 * })
 */
class Interest {
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", unique=TRUE, nullable=FALSE)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="string")
	 */
	private $description;

	/**
	 * @var array
	 *
	 * @ORM\OneToMany(targetEntity="Subscription", mappedBy="subscriber", cascade={"persist", "remove"}, orphanRemoval=TRUE)
	 */
	private $subscribers;

	public function __construct() {
		$this->subscribers = new ArrayCollection();
	}

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Interest
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Interest
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add subscriber
     *
     * @param \Subscribr\Entity\Subscription $subscriber
     *
     * @return Interest
     */
    public function addSubscriber(\Subscribr\Entity\Subscription $subscriber)
    {
        $this->subscribers[] = $subscriber;

        return $this;
    }

    /**
     * Remove subscriber
     *
     * @param \Subscribr\Entity\Subscription $subscriber
     */
    public function removeSubscriber(\Subscribr\Entity\Subscription $subscriber)
    {
        $this->subscribers->removeElement($subscriber);
    }

    /**
     * Get subscribers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubscribers()
    {
        return $this->subscribers;
    }
}
