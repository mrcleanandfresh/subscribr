<?php
/**
 * Created by PhpStorm.
 * User: Kevin Dench
 * Date: 11/28/2016
 * Time: 3:12 PM
 */

namespace Subscribr\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Subscription
 * @ORM\Entity()
 * @ORM\Table(name="subscription")
 */
class Subscription {
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var Interest
	 *
	 * @ORM\ManyToOne(targetEntity="Interest", inversedBy="subscribers")
	 * @ORM\JoinColumn(name="interest_id", referencedColumnName="id", nullable=FALSE)
	 */
	private $interest;

	/**
	 * @var Subscriber
	 *
	 * @ORM\ManyToOne(targetEntity="Subscriber", inversedBy="interests")
	 * @ORM\JoinColumn(name="subscriber_id", referencedColumnName="id", nullable=FALSE)
	 */
	private $subscriber;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="interest_date", type="datetime")
	 */
	private $subscribed_on;

	public function __construct() {
		$this->subscribed_on = new \DateTime("now");
	}

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subscribedOn
     *
     * @param \DateTime $subscribedOn
     *
     * @return Subscription
     */
    public function setSubscribedOn($subscribedOn)
    {
        $this->subscribed_on = $subscribedOn;

        return $this;
    }

    /**
     * Get subscribedOn
     *
     * @return \DateTime
     */
    public function getSubscribedOn()
    {
        return $this->subscribed_on;
    }

    /**
     * Set interest
     *
     * @param \Subscribr\Entity\Interest $interest
     *
     * @return Subscription
     */
    public function setInterest(\Subscribr\Entity\Interest $interest)
    {
        $this->interest = $interest;

        return $this;
    }

    /**
     * Get interest
     *
     * @return \Subscribr\Entity\Interest
     */
    public function getInterest()
    {
        return $this->interest;
    }

    /**
     * Set subscriber
     *
     * @param \Subscribr\Entity\Subscriber $subscriber
     *
     * @return Subscription
     */
    public function setSubscriber(\Subscribr\Entity\Subscriber $subscriber)
    {
        $this->subscriber = $subscriber;

        return $this;
    }

    /**
     * Get subscriber
     *
     * @return \Subscribr\Entity\Subscriber
     */
    public function getSubscriber()
    {
        return $this->subscriber;
    }
}
