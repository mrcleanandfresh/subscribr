<?php
namespace Subscribr;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;


/**
 * Class Subscribr
 * @package Subscribr
 */
class Subscribr {
	/**
	 * @var bool
	 */
	private static $isDevMode = true;
	/**
	 * @var array
	 */
	private static $connection = array(
	    'host'     => '127.0.0.1',
		'driver'   => 'pdo_mysql',
		'user'     => 'root',
		'password' => 'password',
		'dbname'   => 'subscribr'
	);
	/**
	 * @var
	 */
	private static $entityManager;


	/**
	 * @return EntityManager
	 */
	public static function getEntityManager() {
        $entities = array(
            __DIR__.DIRECTORY_SEPARATOR."Entity"
        );
		$config = Setup::createAnnotationMetadataConfiguration( $entities, true, null, null, false );
		return EntityManager::create( self::$connection, $config);
	}
}