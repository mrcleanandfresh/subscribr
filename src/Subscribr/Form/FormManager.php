<?php
namespace Subscribr\Form;

abstract class FormManager {
	const RESPONSE_ALREADY_SENT  = "already_sent";
	const RESPONSE_SUCCESS       = "success";
	const RESPONSE_INVALID_EMAIL = "invalid_email";

	//Error handling
	const VALIDATE_SUCCESS = "success";
	const VALIDATE_WARNING = "warning";
	const VALIDATE_ERROR   = "error";

	protected function getMessage($message) {
		switch ($message) {
			case self::RESPONSE_ALREADY_SENT :
				return "Thank you for reaching out again, but it looks like you already signed up.";
				break;
			case self::RESPONSE_SUCCESS :
				return "Thank you for reaching out, we'll be in touch shortly.";
				break;
			case self::RESPONSE_INVALID_EMAIL :
				return "The Email address you entered was invalid.";
				break;
			default :
				return "Please fill out the form to get in touch with us. All fields are required.";
		}
	}

	protected function generateMessage($type, $message_code) {
		//do something with the $type here.
		//any other logic too.
		return $this->getMessage( $message_code );
	}
}